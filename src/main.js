import Vue from "vue";
import Vuelidate from 'vuelidate'
import Paginate from 'vuejs-paginate'
import App from "./App.vue";
import router from './router'
import store from './store'
import dateFilter from '@/filters/date.filter'
import currencyFilter from '@/filters/currency.filter'
import tooltipDirective from '@/directives/tooltip.directive'
import messagePlugin from '@/utils/message.plugin'
import Loader from '@/components/app/Loader'
import './registerServiceWorker'
import 'materialize-css/dist/js/materialize.min'

import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'

Vue.config.productionTip = false;

Vue.use(messagePlugin)
Vue.use(Vuelidate)
Vue.filter('date', dateFilter)
Vue.filter('currency', currencyFilter)
Vue.directive('tooltip', tooltipDirective)
Vue.component('Loader', Loader)
Vue.component('Paginate', Paginate)


firebase.initializeApp({
  apiKey: "AIzaSyAeOlh_GRefyuB642hUcppypXxMDdB8JUM",
  authDomain: "dogout-936c8.firebaseapp.com",
  databaseURL: "https://dogout-936c8.firebaseio.com",
  projectId: "dogout-936c8",
  storageBucket: "dogout-936c8.appspot.com",
  messagingSenderId: "182421878525",
  appId: "1:182421878525:web:575d16f95b5b9109d87094",
  measurementId: "G-ENG3EG0SPZ"
})

let app

firebase.auth().onAuthStateChanged(() => {
  if (!app) {
    app = new Vue({
      router,
      store,
      render: h => h(App)
    }).$mount('#app')
  }
})