/* eslint-disable */
import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    meta: {
      layout: "main",
    },
    component: () => import("@/views/OffersListPage.vue"),
  },
  {
    path: "/login",
    name: "login",
    component: () => import("@/views/LoginPage.vue"),
  },
  {
    path: "/register",
    name: "register",
    component: () => import("@/views/RegisterPage.vue"),
  },
  {
    path: "/profile",
    name: "profile-page",
    meta: {
      layout: "main",
      auth: true,
    },
    component: () => import("@/views/ProfilePage.vue"),
  },
  {
    path: "/add-offer",
    name: "OffersList",
    meta: {
      layout: "main",
      auth: true,
    },
    component: () => import("@/views/AddOfferPage.vue"),
  },
  {
    path: "/detail/:id",
    name: "detail",
    meta: {
      layout: "main",
    },
    component: () => import("@/views/DetailPage.vue"),
  },
  {
    path: "/about",
    name: "About",
    component: () => import("@/views/About.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
