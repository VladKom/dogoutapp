import firebase from 'firebase/app'

export default {
    actions: {
        async createReview({
                commit
            },
            review
        ) {
            try {
                console.log(review)
                return await firebase.database().ref(`/reviews/`).push(review)
            } catch (e) {
                commit('setError', e)
                throw e
            }
        },

        async fetchReviewsUserAuthor({
            commit
        }, recived_uid) {
            try {
                console.log('recived_uid', recived_uid)
                let reviews = (await firebase.database().ref(`/reviews/`).orderByChild("author_review_id").equalTo(recived_uid).once('value')).val() || {}

                console.log('reviews by user', reviews)
                return Object.keys(reviews).map(key => ({
                    ...reviews[key],
                    id: key
                }))
            } catch (e) {

                commit('setError', e)
                console.log(e)
                throw e

            }
        },
        async fetchReviewsUserRecipient({
            commit
        }, recived_uid) {
            try {
                console.log('recived_uid', recived_uid)
                let reviews = (await firebase.database().ref(`/reviews/`).orderByChild("recipient").equalTo(recived_uid).once('value')).val() || {}

                console.log('reviews by user', reviews)
                return Object.keys(reviews).map(key => ({
                    ...reviews[key],
                    id: key
                }))
            } catch (e) {

                commit('setError', e)
                console.log(e)
                throw e

            }
        },
    },
}