import firebase from 'firebase/app'

export default {
  actions: {
    async createOffer({
      commit
    }, offer) {
      try {
        return await firebase.database().ref(`/offers/`).push(offer)
      } catch (e) {
        commit('setError', e)
        throw e
      }
    },

    async fetchLocations({
      commit
    }) {
      try {
        const locations = (await firebase.database().ref(`/locations/`).once('value')).val() || {}
        return Object.keys(locations).map(key => ({
          ...locations[key],
          id: key
        }))
      } catch (e) {
        commit('setError', e)
        throw e
      }
    },
    async fetchTypes({
      commit
    }) {
      try {
        const types = (await firebase.database().ref(`/offer_types/`).once('value')).val() || {}
        return Object.keys(types).map(key => ({
          ...types[key],
          id: key
        }))
      } catch (e) {
        commit('setError', e)
        throw e
      }
    },
    async fetchUsers({
      commit
    }) {
      try {
        const users = (await firebase.database().ref(`/users/`).once('value')).val() || {}
        return Object.keys(users).map(key => ({
          ...users[key],
          id: key
        }))
      } catch (e) {
        commit('setError', e)
        throw e
      }
    },
    async fetchUserInfo({
      dispatch,
      commit
    }) {
      try {
        const uid = await dispatch('getUid')
        const user = (await firebase.database().ref(`/users/${uid}/info`).once('value')).val() || {}
        return {
          ...user
        }
      } catch (e) {
        commit('setError', e)
        throw e
      }
    },
    async fetchAllOffers({
      commit
    }) {
      try {
        const offers = (await firebase.database().ref(`/offers/`).once('value')).val() || {}
        return Object.keys(offers).map(key => ({
          ...offers[key],
          id: key
        }))
      } catch (e) {
        commit('setError', e)
        throw e
      }
    },

    async fetchOffersByType({
      commit
    }, recived_type) {
      try {
        console.log('recived_type', recived_type)
        let offers = (await firebase.database().ref(`/offers/`).orderByChild("type").equalTo(recived_type).once('value')).val() || {}

        console.log('offers by type', offers)
        return Object.keys(offers).map(key => ({
          ...offers[key],
          id: key
        }))
      } catch (e) {
        commit('setError', e)
        throw e
      }
    },
    async fetchOffersByLocation({
      commit
    }, recived_location) {
      try {
        console.log('recived_location', recived_location)
        let offers = (await firebase.database().ref(`/offers/`).orderByChild("location").equalTo(recived_location).once('value')).val() || {}

        console.log('offers by location', offers)
        return Object.keys(offers).map(key => ({
          ...offers[key],
          id: key
        }))
      } catch (e) {
        commit('setError', e)
        throw e
      }
    },
    async fetchOffersByUser({
      commit
    }, recived_uid) {
      try {
        console.log('recived_uid', recived_uid)
        let offers = (await firebase.database().ref(`/offers/`).orderByChild("author_id").equalTo(recived_uid).once('value')).val() || {}

        console.log('offers by user', offers)
        return Object.keys(offers).map(key => ({
          ...offers[key],
          id: key
        }))
      } catch (e) {

        commit('setError', e)
        console.log(e)
        throw e

      }
    },
    async fetchOfferById({
      commit
    }, id) {
      try {
        const offer = (await firebase.database().ref(`/offers/`).child(id).once('value')).val() || {}
        return {
          ...offer,
          id
        }
      } catch (e) {
        commit('setError', e)
        throw e
      }
    }
  }
}